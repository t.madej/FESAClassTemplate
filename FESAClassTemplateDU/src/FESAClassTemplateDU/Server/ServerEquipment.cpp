#include <FESAClassTemplateDU/Server/ServerEquipment.h>
#include <FESAClassTemplate/GeneratedCode/ServiceLocator.h>
namespace FESAClassTemplateDU
{

ServerEquipment::ServerEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol) : ServerEquipmentGen(serviceLocatorCol)
{
}

ServerEquipment::~ServerEquipment ()
{
}

void ServerEquipment:: specificInit () 
{
}

}
