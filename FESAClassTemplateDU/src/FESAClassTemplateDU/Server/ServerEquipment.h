#ifndef FESAClassTemplateDU_SERVER_EQUIPMENT_H_
#define FESAClassTemplateDU_SERVER_EQUIPMENT_H_

#include <fesa-core/Server/AbstractServerEquipment.h>

#include <fesa-core/Exception/FesaException.h>

#include <FESAClassTemplateDU/GeneratedCode/ServerEquipmentGen.h>

namespace FESAClassTemplateDU
{

class ServerEquipment : public ServerEquipmentGen
{
	public:

		ServerEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol);

		virtual  ~ServerEquipment();

		void specificInit();

};
}
#endif
