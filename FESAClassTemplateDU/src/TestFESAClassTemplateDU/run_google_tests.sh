#!/bin/sh
# Script to configure and run google FESA Class tests
#============================================================================
#% SYNOPSIS
#+    ${SCRIPT_NAME} [-d*] [-s] [-i] [-r]
#%
#% DESCRIPTION
#%    This script is used to compile and run google FESA tests
#%
#% OPTIONS
#%    -d0, --FESAClassTemplate_device_name   set name device for FESAClassTemplate

#%    -s, --server_name             set the server name value
#%    -i, --instance_script_run     set the path to instance script runner
#%    -r, --ready_flag              set the flag on, server is running
#%    -h, --help, --man             print this help
#%
#% SOURCE
#%    https://git.gsi.de/t.madej/fesa-test-generator
#============================================================================
usage() { printf "Usage: "; head -50 ${0} | grep "^#+" | sed -e "s/^#+[ ]*//g" -e "s/\${SCRIPT_NAME}/${SCRIPT_NAME}/g" ; }
usagefull() { head -50 ${0} | grep -e "^#[%+-]" | sed -e "s/^#[%+-]//g" -e "s/\${SCRIPT_NAME}/${SCRIPT_NAME}/g" ; }

unset SCRIPT_NAME SCRIPT_OPTS ARRAY_OPTS

SCRIPT_NAME="$(basename $0)"
OptFull=$@
OptNum=$#
BASEDIR=$(pwd)

SCRIPT_OPTS=':d0:s:i:-:h-:r'
typeset -A ARRAY_OPTS
ARRAY_OPTS=(
    [FESAClassTemplate_device_name]="0"

    [server_name]=s
    [instance_script_run]=i
    [ready_flag]=r
    [help]=h
    [man]=h
)

while getopts ${SCRIPT_OPTS} OPTION ; do
    if [[ "x$OPTION" == "x-" ]]; then
        LONG_OPTION=$OPTARG
        LONG_OPTARG=$(echo $LONG_OPTION | grep "=" | cut -d'=' -f2)
        LONG_OPTIND=-1
        [[ "x$LONG_OPTARG" = "x" ]] && LONG_OPTIND=$OPTIND || LONG_OPTION=$(echo $OPTARG | cut -d'=' -f1)
        [[ $LONG_OPTIND -ne -1 ]] && eval LONG_OPTARG="\$$LONG_OPTIND"
        OPTION=${ARRAY_OPTS[$LONG_OPTION]}
        [[ "x$OPTION" = "x" ]] &&  OPTION="?" OPTARG="-$LONG_OPTION"
        
        if [[ $( echo "${SCRIPT_OPTS}" | grep -c "${OPTION}:" ) -eq 1 ]]; then
            if [[ "x${LONG_OPTARG}" = "x" ]] || [[ "${LONG_OPTARG}" = -* ]]; then 
                OPTION=":" OPTARG="-$LONG_OPTION"
            else
                OPTARG="$LONG_OPTARG";
                if [[ $LONG_OPTIND -ne -1 ]]; then
                    [[ $OPTIND -le $Optnum ]] && OPTIND=$(( $OPTIND+1 ))
                    shift $OPTIND
                    OPTIND=1
                fi
            fi
        fi
    fi

    if [[ "x${OPTION}" != "x:" ]] && [[ "x${OPTION}" != "x?" ]] && [[ "${OPTARG}" = -* ]]; then 
        OPTARG="$OPTION" OPTION=":"
    fi

    case "$OPTION" in
        r) flag_ready=true ;;
        d) ;;
        "0") FESAClassTemplate_device_name=${OPTARG} ;;

        s) server_name=${OPTARG}         ;;
        i) run_instance_script=$(realpath -s "${OPTARG}") ;;
        h) usagefull && exit 0             ;;
        :) echo "${SCRIPT_NAME}: -$OPTARG: option requires an argument" >&2 && usage >&2 && exit 99 ;;
        ?) echo "${SCRIPT_NAME}: -$OPTARG: unknown option" >&2 && usage >&2 && exit 99 ;;
    esac
done
shift $((${OPTIND} - 1))
echo@new
#colors
RED='[0;31m'
BLUE='[0;34m'
GREEN='[0;32m'
NC='[0m' # No Color

################################################################################################################################
printf "\n${BLUE}=============Compile Google-Tests Of FESA Class=============${NC}\n"
make test
COMPILE_RESPONSE=$?
if [[ $COMPILE_RESPONSE != "0" ]]; then
    printf  "${RED}=============Compilation Failed (error code: $COMPILE_RESPONSE)=============${NC}"
    exit
fi
printf "\n${GREEN}=============The compilation has just been successful=============${NC}\n"
################################################################################################################################
if [ -z "$run_instance_script" ]; then
    if [ ! $flag_ready ]; then
        read -r -p "Please start your FESA class now. Done? [y/n]: " response
        if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
        then
            continue
        else
            printf "${RED}============FEC not running, script aborted!=============${NC}\n"
            exit
        fi
    fi
else
        FESAClassTemplate_device_name=$(xmllint --xpath "string(//classes/FESAClassTemplate/device-instance/@name)" "$(dirname "$run_instance_script")/"DeviceData_*.instance)

    server_name=$(xmllint --xpath "//information/fec-name/text()" "$(dirname "$run_instance_script")/"DeviceData_*.instance)
    if [[ ! -x $run_instance_script ]]; then
        printf "\n${RED}=============The script in the given path does not exist or is not executable!=============${NC}\n"
        exit
    fi
    cd $(dirname "$run_instance_script")
    $run_instance_script &
    PID_FESA_Instance_Running=$!
    cd $BASEDIR
    sleep 2
    if [[ "" ==  "$PID_FESA_Instance_Running" ]]; then
        printf "\n${RED}========An error occurred while starting the fesa server========${NC}\n"
        exit
    fi
fi
################################################################################################################################
if [ -z "$FESAClassTemplate_device_name" ]; then
    read -r -p "Please provide FESAClassTemplate device name: " FESAClassTemplate_device_name
fi

if [ -z "$server_name" ]; then
    read -r -p "Please provide server name: " server_name
fi
################################################################################################################################
printf "\n${BLUE}=============Run Google-Tests Of FESA Class=============${NC}\n"
/common/home/bel/tmadej/lnx/PROJECTS/FESAClassTemplate/FESAClassTemplateDU/src/TestFESAClassTemplateDU/test/bin/x86_64/gtest_FESAClassTemplateDU-client $server_name $FESAClassTemplate_device_name
TEST_RESPONSE=$?
if [[ $TEST_RESPONSE == "0" ]]
then
    printf "\n${GREEN}=============Google Tests have just Passed=============${NC}\n"
else
    printf "\n${RED}=============Google Tests have just Failed (error code: $TEST_RESPONSE)=============${NC}\n"
fi
################################################################################################################################
if [ -n "$run_instance_script" ]; then
    if [[ "" !=  "$PID_FESA_Instance_Running" ]]; then
        printf "\n${BLUE}========Killing PID $PID_FESA_Instance_Running Of Running FESA Class Instance========${NC}\n"
        kill -9 $PID_FESA_Instance_Running
    fi
fi
################################################################################################################################
printf "\n${GREEN}====================================DONE!======================================${NC}\n"
################################################################################################################################
