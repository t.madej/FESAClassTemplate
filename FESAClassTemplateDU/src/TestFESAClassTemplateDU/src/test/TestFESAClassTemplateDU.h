#ifndef SRC_TEST_FESACLASSTEMPLATEDU_H_
#define SRC_TEST_FESACLASSTEMPLATEDU_H

#include "include/IntegrationTestRDAClient.h"
#include "include/IntegrationTestListener.h"

// googletest include
#include <gtest/gtest.h>

// system includes
#include <string>
#include <iostream>

namespace TestFESAClassTemplateDU
{

std::string FESAClassTemplate_device_name = "";

std::string server_name = "";
const std::string deploy_unit_name = "FESAClassTemplateDU";

class TestFESAClassTemplateDU : public ::testing::Test, public IntegrationTestRDAClient, public IntegrationTestListener
{
public:
    void SetUpDevice(std::string device_name);
    static void SetUpTestCase();
};

void TestFESAClassTemplateDU::SetUpDevice(std::string device_name)
{
    ASSERT_NO_THROW(
        auto_ptr<Data> data = DataFactory::createData();
        executeSet(device_name,"Init", data); //This will fill the fields of the class with data
    );
} // SetUpDevice()

void TestFESAClassTemplateDU::SetUpTestCase()
{
    connectToRemoteServer(deploy_unit_name, server_name);
} // SetUpTestCase()


} // end namespace

#endif /* SRC_TEST_FESACLASSTEMPLATEDU_H_ */
