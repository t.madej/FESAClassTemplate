#include <iostream>
#include "TestFESAClassTemplateDU.h"


int main(int argc, char **argv)
{
    std::cout << "\nRunning main() from gtest_main.cc" << std::endl;
    TestFESAClassTemplateDU::server_name = std::string(argv[1]);
    TestFESAClassTemplateDU::FESAClassTemplate_device_name = std::string(argv[2]);

    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();

}

namespace TestFESAClassTemplateDU
{

// Make your changes below

    TEST_F(TestFESAClassTemplateDU, FESAClassTemplate_Set_Get_Setting)
    {
        SetUpDevice(FESAClassTemplate_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//            auto_ptr<Data> data = DataFactory::createData();
//            data->append("anSettingField", ...);

//            auto_ptr<Data> check_data = data->clone();
//            executeSet(device_name, "Setting", data);

//            std::auto_ptr<AcquiredData> value = executeGet(FESAClassTemplate_device_name, "Setting");
//            ASSERT_TRUE(value->getData().equals(AcquiredData(check_data).getData()));
        );
    }

    TEST_F(TestFESAClassTemplateDU, FESAClassTemplate_Set_Get_Power)
    {
        SetUpDevice(FESAClassTemplate_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//            auto_ptr<Data> data = DataFactory::createData();
//            data->append("power", ...);

//            auto_ptr<Data> check_data = data->clone();
//            executeSet(device_name, "Power", data);

//            std::auto_ptr<AcquiredData> value = executeGet(FESAClassTemplate_device_name, "Power");
//            ASSERT_TRUE(value->getData().equals(AcquiredData(check_data).getData()));
        );
    }

    TEST_F(TestFESAClassTemplateDU, FESAClassTemplate_Get_Status)
    {
        SetUpDevice(FESAClassTemplate_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//            std::auto_ptr<AcquiredData> value = executeGet(FESAClassTemplate_device_name, "Status");
////            std::cout << value->getData().toString() << endl; // Check the result manually

// // This example only show how GSI-Status-Property test can look like
//            int32_t status = value->getData().getInt("status");
//            size_t detailedStatus_size = 2;
//            const bool* detailedStatus  = value->getData().getArrayBool("detailedStatus", detailedStatus_size);
//            const int* detailedStatus_severity  = value->getData().getArrayInt("detailedStatus_severity", detailedStatus_size);
//            const char* const* detailedStatus_labels = value->getData().getArrayString("detailedStatus_labels", detailedStatus_size);
//            int32_t powerState = value->getData().getInt("powerState");
//            int32_t control = value->getData().getInt("control");
//            bool interlock = value->getData().getBool("interlock");
//            bool opReady = value->getData().getBool("opReady");
//            bool modulesReady = value->getData().getBool("modulesReady");

//            ASSERT_EQ(status, fill_with_expected_data);
//            ASSERT_TRUE/FALSE(detailedStatus[0]);
//            ASSERT_TRUE/FALSE(detailedStatus[1]);
//            ASSERT_EQ(detailedStatus_severity[0], fill_with_expected_data);
//            ASSERT_EQ(detailedStatus_severity[1], fill_with_expected_data);
//            ASSERT_STREQ(detailedStatus_labels[0], fill_with_expected_data);
//            ASSERT_STREQ(detailedStatus_labels[1], fill_with_expected_data);
//            ASSERT_EQ(powerState, fill_with_expected_data);
//            ASSERT_EQ(control, fill_with_expected_data);
//            ASSERT_TRUE/FALSE(opReady);
//            ASSERT_TRUE/FALSE(modulesReady);
//            ASSERT_TRUE/FALSE(interlock);

        );
    }

    TEST_F(TestFESAClassTemplateDU, FESAClassTemplate_Get_ModuleStatus)
    {
        SetUpDevice(FESAClassTemplate_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//            std::auto_ptr<AcquiredData> value = executeGet(FESAClassTemplate_device_name, "ModuleStatus");
////            std::cout << value->getData().toString() << endl; // Check the result manually

// // This example only show how GSI-ModuleStatus-Property test can look like
//            size_t moduleStatus_size = 2;
//            const int* moduleStatus  = value->getData().getArrayInt("moduleStatus", moduleStatus_size);
//            ASSERT_EQ(moduleStatus[0], fill_with_expected_data);
//            ASSERT_EQ(moduleStatus[1], fill_with_expected_data);

//            const char * const *  moduleStatus_labels = value->getData().getArrayString("moduleStatus_labels", moduleStatus_size);
//            ASSERT_STREQ(moduleStatus_labels[0], fill_with_expected_data); 
//            ASSERT_STREQ(moduleStatus_labels[1], fill_with_expected_data);

        );
    }

    TEST_F(TestFESAClassTemplateDU, FESAClassTemplate_Get_Acquisition)
    {
        SetUpDevice(FESAClassTemplate_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//            std::auto_ptr<AcquiredData> value = executeGet(FESAClassTemplate_device_name, "Acquisition");
////            std::cout << value->getData().toString() << endl; // Check the result manually

// // This example only show how GSI-Acquisition-Property test can look like

//            int32_t processIndexfield = value->getData().getInt("processIndex");
//            ASSERT_EQ(processIndexfield, ...);
//            int32_t sequenceIndexfield = value->getData().getInt("sequenceIndex");
//            ASSERT_EQ(sequenceIndexfield, ...);
//            int32_t chainIndexfield = value->getData().getInt("chainIndex");
//            ASSERT_EQ(chainIndexfield, ...);
//            int32_t eventNumberfield = value->getData().getInt("eventNumber");
//            ASSERT_EQ(eventNumberfield, ...);
//            int32_t timingGroupIDfield = value->getData().getInt("timingGroupID");
//            ASSERT_EQ(timingGroupIDfield, ...);
//            int64_t acquisitionStampfield = value->getData().getLong("acquisitionStamp");
//            ASSERT_EQ(acquisitionStampfield, ...);
//            int64_t eventStampfield = value->getData().getLong("eventStamp");
//            ASSERT_EQ(eventStampfield, ...);
//            int64_t processStartStampfield = value->getData().getLong("processStartStamp");
//            ASSERT_EQ(processStartStampfield, ...);
//            int64_t sequenceStartStampfield = value->getData().getLong("sequenceStartStamp");
//            ASSERT_EQ(sequenceStartStampfield, ...);
//            int64_t chainStartStampfield = value->getData().getLong("chainStartStamp");
//            ASSERT_EQ(chainStartStampfield, ...);
        );
    }

    TEST_F(TestFESAClassTemplateDU, FESAClassTemplate_Get_Version)
    {
        SetUpDevice(FESAClassTemplate_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//            std::auto_ptr<AcquiredData> value = executeGet(FESAClassTemplate_device_name, "Version");
////            std::cout << value->getData().toString() << endl; // Check the result manually

//            ASSERT_FALSE(strcmp(value->getData().getString("fesaVersion"), "7.3.0"));
//            ASSERT_FALSE(strcmp(value->getData().getString("deployUnitVersion"), "0.1.0"));
//            ASSERT_FALSE(strcmp(value->getData().getString("classVersion"), "0.1.0"));

        );
    }

    TEST_F(TestFESAClassTemplateDU, FESAClassTemplate_RT_ACTION_StatusUpdateAction)
    {
        SetUpDevice(FESAClassTemplate_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//                TestListenerSharedPtr replyHandler(new IntegrationTestListener);
//// Below you will find only a test suggestion !!!

//                for(int i = 0 ; i < 5 ; i++)
//                {
//                    std::cout << "Step real-time action # " << i << std::endl;
//                    sleep(1);
//                    std::auto_ptr<AcquiredData> value = executeGet(device_name, "Fill Property Name");
//// The cout below allow you to manually check the operation
//                    std::cout << "Data: " << value->getData().toString() << std::endl;
//// An appropriate assertion is suggested(example below)
////                        ASSERT_TRUE(replyHandler->receivedData_->getData().getBool("..."));
//                }


        );
    }

    TEST_F(TestFESAClassTemplateDU, FESAClassTemplate_RT_ACTION_RealTimeAction)
    {
        SetUpDevice(FESAClassTemplate_device_name);
//// Fill up as needed!
        ASSERT_NO_THROW(
//                TestListenerSharedPtr replyHandler(new IntegrationTestListener);
//// Below you will find only a test suggestion !!!

//                SubscriptionSharedPtr subscription;

//                subscription = subscribe(FESAClassTemplate_device_name,"Status",replyHandler);

//                for(int i = 0 ; i < 5 ; i++)
//                {
//                    std::cout << "Subscription run # " << i << std::endl;
//                    sleep(1);
//                    std::cout << "Data available: " << replyHandler->dataAvailable_ << std::endl;

//                    if( replyHandler->dataAvailable_ ){
//// The cout below allows you to manually check the operation
//                        std::cout << "Data: " << replyHandler->receivedData_->getData().toString() << std::endl;
//                        std::cout << "Context: " << replyHandler->receivedData_->getContext().toString() << std::endl;
//// An appropriate assertion is suggested(example below)
//                        ASSERT_FALSE(replyHandler->receivedData_->getData().getBool("..."));
//                    } else {
//                        std::cout << "Subscription: no data received" << std::endl;
//                        std::cout << "Please make sure the property Status is automatically notified." << std::endl;
//                    }
//                }
//                unsubscribe(subscription);

//                subscription = subscribe(FESAClassTemplate_device_name,"Acquisition",replyHandler);

//                for(int i = 0 ; i < 5 ; i++)
//                {
//                    std::cout << "Subscription run # " << i << std::endl;
//                    sleep(1);
//                    std::cout << "Data available: " << replyHandler->dataAvailable_ << std::endl;

//                    if( replyHandler->dataAvailable_ ){
//// The cout below allows you to manually check the operation
//                        std::cout << "Data: " << replyHandler->receivedData_->getData().toString() << std::endl;
//                        std::cout << "Context: " << replyHandler->receivedData_->getContext().toString() << std::endl;
//// An appropriate assertion is suggested(example below)
//                        ASSERT_FALSE(replyHandler->receivedData_->getData().getBool("..."));
//                    } else {
//                        std::cout << "Subscription: no data received" << std::endl;
//                        std::cout << "Please make sure the property Acquisition is automatically notified." << std::endl;
//                    }
//                }
//                unsubscribe(subscription);

//                subscription = subscribe(FESAClassTemplate_device_name,"ModuleStatus",replyHandler);

//                for(int i = 0 ; i < 5 ; i++)
//                {
//                    std::cout << "Subscription run # " << i << std::endl;
//                    sleep(1);
//                    std::cout << "Data available: " << replyHandler->dataAvailable_ << std::endl;

//                    if( replyHandler->dataAvailable_ ){
//// The cout below allows you to manually check the operation
//                        std::cout << "Data: " << replyHandler->receivedData_->getData().toString() << std::endl;
//                        std::cout << "Context: " << replyHandler->receivedData_->getContext().toString() << std::endl;
//// An appropriate assertion is suggested(example below)
//                        ASSERT_FALSE(replyHandler->receivedData_->getData().getBool("..."));
//                    } else {
//                        std::cout << "Subscription: no data received" << std::endl;
//                        std::cout << "Please make sure the property ModuleStatus is automatically notified." << std::endl;
//                    }
//                }
//                unsubscribe(subscription);

        );
    }
// End of making changes

} // end namespace
