#ifndef INTEGRATION_TEST_RDA_CLIENT_H_
#define INTEGRATION_TEST_RDA_CLIENT_H_

#include <cmw-rda3/client/service/ClientServiceBuilder.h>

using namespace std;
using namespace cmw::util;
using namespace cmw::data;
using namespace cmw::rda3::client;
using namespace cmw::rda3::common;

// system includes
#include <string>
#include <iostream>
#include <unistd.h>

class IntegrationTestRDAClient
{
private:


public:

    static std::string server_name_;
    static auto_ptr<ClientService> clientService_;
    static void setServerName(std::string serverName);
    static void connectToServer(std::string serverName);
    static void connectToLocalServer(std::string deploy_unit_name);
    static void connectToRemoteServer(std::string deploy_unit_name, std::string hostname);

    static void printException(const RdaException& ex);
    static void executeSet(std::string device_name, std::string property_name, auto_ptr<Data> data , std::string cycleName = "");
    static void executeFilteredSet( std::string device_name, std::string property_name, auto_ptr<Data> data, auto_ptr<Data> filter, std::string cycleName="" );
    static std::auto_ptr<AcquiredData> executeGet(std::string device_name, std::string property_name, std::string cycleName = "");
    static std::auto_ptr<AcquiredData> executeFilteredGet(std::string device_name, std::string property_name, auto_ptr<Data> filter, std::string cycleName = "");
    static SubscriptionSharedPtr subscribe(std::string device_name, std::string property_name, NotificationListenerSharedPtr replyHandler, std::string cycleName = "");
    static bool unsubscribe(SubscriptionSharedPtr& sub);

};

//init static members
std::string IntegrationTestRDAClient::server_name_ = "";
auto_ptr<ClientService> IntegrationTestRDAClient::clientService_ = (auto_ptr<ClientService>)NULL;

void IntegrationTestRDAClient::setServerName(std::string serverName)
{
    server_name_ = serverName;
    std::cout << "Used server_name: " << server_name_ << std::endl;
}

void IntegrationTestRDAClient::connectToServer(std::string serverName)
{
    server_name_ = serverName;
    clientService_ = ClientServiceBuilder::newInstance()->build();
    std::cout << "Used server_name: " << server_name_ << std::endl;
}

void IntegrationTestRDAClient::connectToLocalServer(std::string deploy_unit_name)
{
    char hostname[128];
    gethostname(hostname, sizeof hostname);
    // FIXME does not work properly because hostname() returns hostname including network domain
    connectToServer(deploy_unit_name + "." + hostname );
}

void IntegrationTestRDAClient::connectToRemoteServer(std::string deploy_unit_name, std::string hostname)
{
    connectToServer(deploy_unit_name + "." + hostname.c_str() );
}

void IntegrationTestRDAClient::printException(const RdaException& ex)
{
    std::cout << "Exception caught during Test:" << std::endl;
    cout << ex.what() << endl;
    std::vector<std::string> stack = ex.getStack();
    std::vector<std::string>::iterator iter;
    for ( iter = stack.begin(); iter != stack.end(); iter++ )
    {
        std::cout << *iter << std::endl;
    }
}

void IntegrationTestRDAClient::executeSet( std::string device_name, std::string property_name, auto_ptr<Data> data, std::string cycleName )
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
        auto_ptr<Data> filters = DataFactory::createData();
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filters);
        accessPoint.set(data, context);
    }
    catch(const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
    catch(...)
    {
        std::cout << "Unknown Exception in executeSet" << std::endl;
        throw;
    }
}

void IntegrationTestRDAClient::executeFilteredSet( std::string device_name, std::string property_name, auto_ptr<Data> data, auto_ptr<Data> filter, std::string cycleName )
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filter);
        accessPoint.set(data, context);
    }
    catch(const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
    catch(...)
    {
        std::cout << "Unknown Exception in executeFilteredSet" << std::endl;
        throw;
    }
}

std::auto_ptr<AcquiredData> IntegrationTestRDAClient::executeGet(std::string device_name, std::string property_name, std::string cycleName)
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
        auto_ptr<Data> filters = DataFactory::createData();
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filters);
        return accessPoint.get( context );
    }
    catch(const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
    catch(...)
    {
        std::cout << "Unknown Exception in executeGet" << std::endl;
        throw;
    }
}

std::auto_ptr<AcquiredData> IntegrationTestRDAClient::executeFilteredGet(std::string device_name, std::string property_name, auto_ptr<Data> filter, std::string cycleName)
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filter);
        return accessPoint.get( context );
    }
    catch(const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
    catch(...)
    {
        std::cout << "Unknown Exception in executeFilteredGet" << std::endl;
        throw;
    }
}

// Starts a subscription to the specified property
SubscriptionSharedPtr IntegrationTestRDAClient::subscribe(std::string device_name, std::string property_name, NotificationListenerSharedPtr replyHandler,  std::string cycleName)
{
    try
    {
        AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name,server_name_);
        auto_ptr<Data> filters = DataFactory::createData();
        std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filters);
        SubscriptionSharedPtr sub = accessPoint.subscribe(context, replyHandler);
        return sub;
    }
    catch (const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }

}

bool IntegrationTestRDAClient::unsubscribe(SubscriptionSharedPtr& sub)
{
    try
    {
        sub->unsubscribe();
        return true;
    }
    catch (const RdaException& ex)
    {
        printException(ex);
        throw ex;
    }
}

#endif // INTEGRATION_TEST_RDA_CLIENT_H_
