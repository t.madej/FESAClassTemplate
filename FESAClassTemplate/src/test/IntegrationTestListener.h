// Copyright GSI 2014
#ifndef INTEGRATION_TEST_LISTENER_H_
#define INTEGRATION_TEST_LISTENER_H_
#include <cmw-rda3/client/service/ClientServiceBuilder.h>
#include <cmw-rda3/common/config/Defs.h>
using namespace cmw::util;
using namespace cmw::data;
using namespace cmw::rda3::client;
using namespace cmw::rda3::common;
// The callback object that is used to handle subscription results

class IntegrationTestListener: public NotificationListener
{
    public:
        IntegrationTestListener()
        {
            dataAvailable_ = false;
            numberOfValidUpdates_ = 0;
            //auto_ptr<Data> data = DataFactory::createData();
            receivedData_.reset(new AcquiredData(DataFactory::createData()));
            //TODO: fill auto-ptr with empty data to avoid seg-faults
        }
        virtual void dataReceived(const Subscription & subscription, std::auto_ptr<AcquiredData> value, UpdateType updateType)
        {
            if( updateType == UT_NORMAL || updateType == UT_IMMEDIATE_UPDATE )
            {
                //To allow easy debugging:
                if (value.get() != NULL)
                {
                    numberOfValidUpdates_++;
                    dataAvailable_ = true;
                    receivedData_ = value;
                    //For debugging
                    //std::cout << receivedData_->getData().toString() << std::endl;
                }
                else
                {
                    std::string error = "IntegrationTestListener: No Data available for this update";
                    std::cout << error << std::endl;
                    throw error;
                }
            }
            else if(updateType == UT_FIRST_UPDATE) //we dont care for the first-update of subscriptions
            {
                std::cout << "IntegrationTestListener - UT_FIRST_UPDATE was ignored" << std::endl;
            }
        }
        void errorReceived(const Subscription & subscription, std::auto_ptr<RdaException> exception, UpdateType updateType)
        {
            if( updateType != UT_FIRST_UPDATE )
            {
                std::cout << "error received in SubscriptionReportHandler" << std::endl;
                std::cout << exception->what() << std::endl;
                std::cout << std::endl;
            }
        }
        std::auto_ptr<AcquiredData> receivedData_;
        bool dataAvailable_;
        unsigned int numberOfValidUpdates_;
};
typedef shared_ptr<IntegrationTestListener>::type TestListenerSharedPtr;
#endif /* INTEGRATION_TEST_LISTENER_H_ */
