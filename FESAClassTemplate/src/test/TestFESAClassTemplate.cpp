#include "TestFESAClassTemplate.h"


namespace TestFESAClassTemplate
{

// Make your changes below

    TEST_F(TestFESAClassTemplate, ConnectionWithServer)
	{
		ASSERT_NO_THROW(
			const ClientConnection & client_conn = clientService_->getConnection(server_name_);
            ASSERT_EQ(server_name_, client_conn.getName());
		);
// Suggestions to check the correct connection to the server
		ASSERT_NO_THROW(
			auto_ptr<Data> data = DataFactory::createData();
			executeSet(device_name,"Init", data); //This will fill the fields of the class with data
			sleep(1);
            std::auto_ptr<AcquiredData> value = executeGet(device_name, "Status");
        );
	}

    TEST_F(TestFESAClassTemplate, SetAndGetSetting)
    {
//// Fill up as needed!
        ASSERT_NO_THROW(
            auto_ptr<Data> data = DataFactory::createData();
            data->append("anSettingField", 157);

            auto_ptr<Data> check_data = data->clone();
            executeSet(device_name, "Setting", data);

            std::auto_ptr<AcquiredData> value = executeGet(device_name, "Setting");
            ASSERT_TRUE(value->getData().equals(AcquiredData(check_data).getData()));
        );
    }

    TEST_F(TestFESAClassTemplate, SetAndGetPower)
    {
//// Fill up as needed!
        ASSERT_NO_THROW(
            auto_ptr<Data> data = DataFactory::createData();
            data->append("power", DEVICE_POWER::ON);

            auto_ptr<Data> check_data = data->clone();
            executeSet(device_name, "Power", data);

            std::auto_ptr<AcquiredData> value = executeGet(device_name, "Power");
            ASSERT_TRUE(value->getData().equals(AcquiredData(check_data).getData()));
        );
    }

    TEST_F(TestFESAClassTemplate, GetStatus)
    {
//// Fill up as needed!
        ASSERT_NO_THROW(

            auto_ptr<Data> data = DataFactory::createData();
            executeSet(device_name,"Init", data); //This will fill the fields of the class with data
            sleep(1);

            std::auto_ptr<AcquiredData> value = executeGet(device_name, "Status");
//            std::cout << value->getData().toString() << endl; // Check the result manually

 // This example only show how GSI-Status-Property test can look like
            int32_t status = value->getData().getInt("status");
            size_t detailedStatus_size = 2;
            const bool* detailedStatus  = value->getData().getArrayBool("detailedStatus", detailedStatus_size);
            const int* detailedStatus_severity  = value->getData().getArrayInt("detailedStatus_severity", detailedStatus_size);
            const char* const* detailedStatus_labels = value->getData().getArrayString("detailedStatus_labels", detailedStatus_size);
            int32_t powerState = value->getData().getInt("powerState");
            int32_t control = value->getData().getInt("control");
            bool interlock = value->getData().getBool("interlock");
            bool opReady = value->getData().getBool("opReady");
            bool modulesReady = value->getData().getBool("modulesReady");

            ASSERT_EQ(status, 3);
            ASSERT_FALSE(detailedStatus[0]);
            ASSERT_TRUE(detailedStatus[1]);
            ASSERT_EQ(detailedStatus_severity[0], 0);
            ASSERT_EQ(detailedStatus_severity[1], 0);
            ASSERT_STREQ(detailedStatus_labels[0], "aStatusLabel1");
            ASSERT_STREQ(detailedStatus_labels[1], "aStatusLabel2");
            ASSERT_EQ(powerState, 1);
            ASSERT_EQ(control, 1);
            ASSERT_FALSE(opReady);
            ASSERT_TRUE(modulesReady);
            ASSERT_TRUE(interlock);

        );
    }

    TEST_F(TestFESAClassTemplate, GetModuleStatus)
    {
//// Fill up as needed!
        ASSERT_NO_THROW(

            auto_ptr<Data> data = DataFactory::createData();
            executeSet(device_name,"Init", data); //This will fill the fields of the class with data
            sleep(1);

            std::auto_ptr<AcquiredData> value = executeGet(device_name, "ModuleStatus");
//            std::cout << value->getData().toString() << endl; // Check the result manually

 // This example only show how GSI-ModuleStatus-Property test can look like
            size_t moduleStatus_size = 2;
            const int* moduleStatus  = value->getData().getArrayInt("moduleStatus", moduleStatus_size);
            ASSERT_EQ(moduleStatus[0], 4);
            ASSERT_EQ(moduleStatus[1], 1);

            const char * const *  moduleStatus_labels = value->getData().getArrayString("moduleStatus_labels", moduleStatus_size);
            ASSERT_STREQ(moduleStatus_labels[0], "myModule1");
            ASSERT_STREQ(moduleStatus_labels[1], "myModule2");

        );
    }

    TEST_F(TestFESAClassTemplate, GetAcquisition)
    {
//// Fill up as needed!
        ASSERT_NO_THROW(

            auto_ptr<Data> data = DataFactory::createData();
            executeSet(device_name,"Init", data); //This will fill the fields of the class with data
            sleep(1);

            std::auto_ptr<AcquiredData> value = executeGet(device_name, "Acquisition");
            std::cout << value->getData().toString() << endl; // Check the result manually

// // This example only show how GSI-Acquisition-Property test can look like

            int32_t processIndexfield = value->getData().getInt("processIndex");
            ASSERT_EQ(processIndexfield, 0);
            int32_t sequenceIndexfield = value->getData().getInt("sequenceIndex");
            ASSERT_EQ(sequenceIndexfield, 0);
//            int32_t chainIndexfield = value->getData().getInt("chainIndex");
//            ASSERT_EQ(chainIndexfield, 0);
//            int32_t eventNumberfield = value->getData().getInt("eventNumber");
//            ASSERT_EQ(eventNumberfield, 0);
//            int32_t timingGroupIDfield = value->getData().getInt("timingGroupID");
//            ASSERT_EQ(timingGroupIDfield, 0);
//            int64_t acquisitionStampfield = value->getData().getLong("acquisitionStamp");
//            ASSERT_EQ(acquisitionStampfield, 1662533013469384000);
//            int64_t eventStampfield = value->getData().getLong("eventStamp");
//            ASSERT_EQ(eventStampfield, 0);
            int64_t processStartStampfield = value->getData().getLong("processStartStamp");
            ASSERT_EQ(processStartStampfield, 1234);
            int64_t sequenceStartStampfield = value->getData().getLong("sequenceStartStamp");
            ASSERT_EQ(sequenceStartStampfield, 1234);
//            int64_t chainStartStampfield = value->getData().getLong("chainStartStamp");
//            ASSERT_EQ(chainStartStampfield, 0);
        );
    }

    TEST_F(TestFESAClassTemplate, GetVersion)
    {
//// Fill up as needed!
        ASSERT_NO_THROW(

            std::auto_ptr<AcquiredData> value = executeGet(device_name, "Version");
//            std::cout << value->getData().toString() << endl; // Check the result manually

            ASSERT_FALSE(strcmp(value->getData().getString("fesaVersion"), "7.3.0"));
            ASSERT_FALSE(strcmp(value->getData().getString("deployUnitVersion"), "0.1.0"));
            ASSERT_FALSE(strcmp(value->getData().getString("classVersion"), "0.1.0"));

        );
    }

    TEST_F(TestFESAClassTemplate, RT_ACTION_StatusUpdateAction)
    {
// Fill up as needed!
        auto_ptr<Data> data = DataFactory::createData();
        executeSet(device_name,"Init", data); //This will fill the fields of the class with data
        sleep(1);
        ASSERT_NO_THROW(
                TestListenerSharedPtr replyHandler(new IntegrationTestListener);
//// Below you will find only a test suggestion !!!

                for(int i = 0 ; i < 5 ; i++)
                {
                    std::cout << "Step real-time action # " << i << std::endl;
                    sleep(1);
                    std::auto_ptr<AcquiredData> value = executeGet(device_name, "Status");
// The cout below allow you to manually check the operation
                    std::cout << "Data: " << value->getData().toString() << std::endl;
// An appropriate assertion is suggested(example below)
//                    ASSERT_TRUE(replyHandler->receivedData_->getData().getBool("..."));
                }



        );
    }

    TEST_F(TestFESAClassTemplate, RT_ACTION_RealTimeAction)
    {
// Fill up as needed!
    	SubscriptionSharedPtr subscription;
        auto_ptr<Data> data = DataFactory::createData();
        executeSet(device_name,"Init", data); //This will fill the fields of the class with data
        sleep(1);
        ASSERT_NO_THROW(
                TestListenerSharedPtr replyHandler(new IntegrationTestListener);
// Below you will find only a test suggestion !!!

                subscription = subscribe(device_name,"Status",replyHandler);

                for(int i = 0 ; i < 5 ; i++)
                {
                    std::cout << "Subscription run # " << i << std::endl;
                    sleep(1);
                    std::cout << "Data available: " << replyHandler->dataAvailable_ << std::endl;

                    if( replyHandler->dataAvailable_ ){
// The cout below allows you to manually check the operation
                        std::cout << "Data: " << replyHandler->receivedData_->getData().toString() << std::endl;
                        std::cout << "Context: " << replyHandler->receivedData_->getContext().toString() << std::endl;
// An appropriate assertion is suggested(example below)
//                        ASSERT_FALSE(replyHandler->receivedData_->getData().getBool("..."));
                    } else {
                        std::cout << "Subscription: no data received" << std::endl;
                        std::cout << "Please make sure the property Status is automatically notified." << std::endl;
                    }
                }
                unsubscribe(subscription);

                subscription = subscribe(device_name,"Acquisition",replyHandler);

                for(int i = 0 ; i < 5 ; i++)
                {
                    std::cout << "Subscription run # " << i << std::endl;
                    sleep(1);
                    std::cout << "Data available: " << replyHandler->dataAvailable_ << std::endl;

                    if( replyHandler->dataAvailable_ ){
// The cout below allows you to manually check the operation
                        std::cout << "Data: " << replyHandler->receivedData_->getData().toString() << std::endl;
                        std::cout << "Context: " << replyHandler->receivedData_->getContext().toString() << std::endl;
// An appropriate assertion is suggested(example below)
//                        ASSERT_FALSE(replyHandler->receivedData_->getData().getBool("..."));
                    } else {
                        std::cout << "Subscription: no data received" << std::endl;
                        std::cout << "Please make sure the property Acquisition is automatically notified." << std::endl;
                    }
                }
                unsubscribe(subscription);

                subscription = subscribe(device_name,"ModuleStatus",replyHandler);

                for(int i = 0 ; i < 5 ; i++)
                {
                    std::cout << "Subscription run # " << i << std::endl;
                    sleep(1);
                    std::cout << "Data available: " << replyHandler->dataAvailable_ << std::endl;

                    if( replyHandler->dataAvailable_ ){
// The cout below allows you to manually check the operation
                        std::cout << "Data: " << replyHandler->receivedData_->getData().toString() << std::endl;
                        std::cout << "Context: " << replyHandler->receivedData_->getContext().toString() << std::endl;
// An appropriate assertion is suggested(example below)
//                        ASSERT_FALSE(replyHandler->receivedData_->getData().getBool("..."));
                    } else {
                        std::cout << "Subscription: no data received" << std::endl;
                        std::cout << "Please make sure the property ModuleStatus is automatically notified." << std::endl;
                    }
                }
                unsubscribe(subscription);


        );
    }
// End of making changes

} // end namespace
