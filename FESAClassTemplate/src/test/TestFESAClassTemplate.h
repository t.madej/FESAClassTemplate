
#ifndef SRC_TEST_FESACLASSTEMPLATE_H_
#define SRC_TEST_FESACLASSTEMPLATE_H_

// googletest include
#include <gtest/gtest.h>
// system includes
#include <iostream>
#include <FESAClassTemplate/GeneratedCode/TypeDefinition.h>
#include <cmw-rda3/client/service/ClientServiceBuilder.h>
#include "IntegrationTestListener.h"

using namespace std;
using namespace cmw::util;
using namespace cmw::data;
using namespace cmw::rda3::client;
using namespace cmw::rda3::common;
using namespace FESAClassTemplate;

namespace TestFESAClassTemplate
{
	const std::string device_name = "FESAClassTemplate_asl740";
	const std::string deploy_unit_name = "FESAClassTemplateDU";

	class TestFESAClassTemplate : public ::testing::Test
	{
	public:
		static void SetUpTestCase();
        static SubscriptionSharedPtr subscribe(std::string device_name, std::string property_name, NotificationListenerSharedPtr replyHandler, std::string cycleName = "");
        static bool unsubscribe(SubscriptionSharedPtr& sub);
		static void printException(const RdaException& ex);
		std::auto_ptr<AcquiredData> executeGet(std::string device_name, std::string property_name, std::string cycleName="");
		void executeSet( std::string device_name, std::string property_name, std::auto_ptr<Data> data, std::string cycleName="");
		static std::string server_name_;
		static std::auto_ptr<ClientService> clientService_;
	private:
		static void connectToServer();
	};

    std::string TestFESAClassTemplate::server_name_ = "FESAClassTemplateDU.asl740";
    std::auto_ptr<ClientService> TestFESAClassTemplate::clientService_ = static_cast<std::auto_ptr<ClientService>>(NULL);

    void TestFESAClassTemplate::SetUpTestCase()
    {
        std::cout << "Trying to connect to " << server_name_ << std::endl;
        connectToServer();

    }

    std::auto_ptr<AcquiredData> TestFESAClassTemplate::executeGet(std::string device_name, std::string property_name, std::string cycleName)
    {
        try
        {
            AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
            std::auto_ptr<Data> filters = DataFactory::createData();
            std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filters);
            return accessPoint.get( context );
        }
        catch(const RdaException& ex)
        {
            printException(ex);
            throw ex;
        }
        catch(...)
        {
            std::cout << "Unknown Exception in executeGet" << std::endl;
            throw;
        }
    }

    void TestFESAClassTemplate::executeSet( std::string device_name, std::string property_name, std::auto_ptr<Data> data, std::string cycleName )
    {
        try
        {
            AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name ,server_name_);
            std::auto_ptr<Data> filters = DataFactory::createData();
            std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filters);
            accessPoint.set(data, context);
        }
        catch(const RdaException& ex)
        {
            printException(ex);
            throw ex;
        }
        catch(...)
        {
            std::cout << "Unknown Exception in executeSet" << std::endl;
            throw;
        }
    }

    void TestFESAClassTemplate::printException(const RdaException& ex)
    {
        std::cout << "Exception caught during Test:" << std::endl;
        cout << ex.what() << endl;
        std::vector<std::string> stack = ex.getStack();
        std::vector<std::string>::iterator iter;
        for ( iter = stack.begin(); iter != stack.end(); iter++ )
        {
            std::cout << *iter << std::endl;
        }
    }

    void TestFESAClassTemplate::connectToServer()
    {
        clientService_ = ClientServiceBuilder::newInstance()->build();
        std::cout << "Used server_name: " << server_name_ << std::endl;
    }

    // Starts a subscription to the specified property
    SubscriptionSharedPtr TestFESAClassTemplate::subscribe(std::string device_name, std::string property_name, NotificationListenerSharedPtr replyHandler,  std::string cycleName)
    {
        try
        {
            AccessPoint & accessPoint = clientService_->getAccessPoint(device_name, property_name,server_name_);
            auto_ptr<Data> filters = DataFactory::createData();
            std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycleName, filters);
            SubscriptionSharedPtr sub = accessPoint.subscribe(context, replyHandler);
            return sub;
        }
        catch (const RdaException& ex)
        {
            printException(ex);
            throw ex;
        }
    }

    bool TestFESAClassTemplate::unsubscribe(SubscriptionSharedPtr& sub)
    {
        try
        {
            sub->unsubscribe();
            return true;
        }
        catch (const RdaException& ex)
        {
            printException(ex);
            throw ex;
        }
    }
} // end namespace

#endif /* SRC_TEST_FESACLASSTEMPLATE_H_ */
