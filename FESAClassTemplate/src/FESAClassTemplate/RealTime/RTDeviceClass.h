
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FESAClassTemplate_RT_DEVICE_CLASS_H_
#define _FESAClassTemplate_RT_DEVICE_CLASS_H_

#include <FESAClassTemplate/GeneratedCode/RTDeviceClassGen.h>

namespace FESAClassTemplate
{

class RTDeviceClass: public RTDeviceClassGen
{
public:
    static RTDeviceClass* getInstance();
    static void releaseInstance();
    void specificInit();
    void specificShutDown();
private:
    RTDeviceClass();
    virtual ~RTDeviceClass();
    static RTDeviceClass* instance_;
};

} // FESAClassTemplate

#endif // _FESAClassTemplate_RT_DEVICE_CLASS_H_
