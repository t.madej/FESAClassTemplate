
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <FESAClassTemplate/RealTime/RTDeviceClass.h>
#include <FESAClassTemplate/GeneratedCode/ServiceLocator.h>
#include <fesa-core/Core/AbstractEvent.h>

#include <cmw-log/Logger.h>

#include <string>
#include <vector>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.FESAClassTemplate.RealTime.RTDeviceClass");

} // namespace

namespace FESAClassTemplate
{

RTDeviceClass* RTDeviceClass::instance_ = NULL;

RTDeviceClass::RTDeviceClass () :
    RTDeviceClassGen()
{
}

RTDeviceClass::~RTDeviceClass()
{
}

RTDeviceClass* RTDeviceClass::getInstance()
{
    if (instance_ == NULL)
    {
        instance_ = new RTDeviceClass();
    }
    return instance_;
}

void RTDeviceClass::releaseInstance()
{
    if (instance_ != NULL) 
    {
        delete instance_;
        instance_ = NULL;
    }
}

// This method is called when the FESA class starts up.
// You can write code that initializes devices in the loop below.
void RTDeviceClass::specificInit()
{
    const Devices& deviceCol = FESAClassTemplateServiceLocator_->getDeviceCollection();
    for (Devices::const_iterator it = deviceCol.begin(); it != deviceCol.end(); ++it)
    {
        try
        {
            Device* device = *it;
            static_cast<void>(device); // This line prevents an "unused variable" warning, it can be removed safely.
            // Write here some code to initialize devices
            
        }
        catch (const fesa::FesaException& exception)
        {
            LOG_ERROR_IF(logger, exception.getMessage());
            // Re-throwing the exception prevents the process from starting up.
            throw;
        }
    }
}

void RTDeviceClass::specificShutDown()
{
    // This method is executed just before a normal shut down of the process.
}

} // FESAClassTemplate
