
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <FESAClassTemplate/GeneratedCode/ServiceLocator.h>
#include <FESAClassTemplate/RealTime/RealTimeAction.h>

#include <cmw-log/Logger.h>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.FESAClassTemplate.RealTime.RealTimeAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "FESAClassTemplate"; \
    diagMsg.name = "RealTimeAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    FESAClassTemplateServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace FESAClassTemplate
{

RealTimeAction::RealTimeAction(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     RealTimeActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
}

RealTimeAction::~RealTimeAction()
{
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void RealTimeAction::execute(fesa::RTEvent* pEvt)
{
    std::cout << "TRACE: " __FILE__ << ": " << __FUNCTION__ << ": Line " << __LINE__ << " START " << std::endl;
    fesa::MultiplexingContext* context = pEvt->getMultiplexingContext();
    static_cast<void>(context); // This line prevents an "unused variable" warning, it can be removed safely.
    const Devices& devices = getFilteredDeviceCollection(pEvt);
    for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it)
    {
        try
        {
            Device* device = *it;
            static_cast<void>(device); // This line prevents an "unused variable" warning, it can be removed safely.
            // Write here some code to process devices
            std::cout << "TRACE: " __FILE__ << ": " << __FUNCTION__ << ": Line " << __LINE__ << ": Init StatusProperty " << std::endl;
            device->status.set(DEVICE_STATUS::ERROR, context);
            bool detailedStatus[DETAILED_STATUS_SIZE] = { false, false, false, false, false};
            device->detailedStatus.set(detailedStatus,DETAILED_STATUS_SIZE,context);
            device->detailedStatus.lower(0, context);
            device->detailedStatus.raise(1, context);
            device->detailedStatus.setBit("aStatusLabel3", false, context);
            device->detailedStatus.set("aStatusLabel4", true, context);
            device->detailedStatus.setCell(4, true, context);
            device->powerState.set(DEVICE_POWER_STATE::ON, context);
            device->control.set(DEVICE_CONTROL::LOCAL, context);
            device->modulesReady.set(true, context);
            device->interlock.set(true, context);
            device->opReady.set(false, context);

            std::cout << "TRACE: " __FILE__ << ": " << __FUNCTION__ << ": Line " << __LINE__ << ": Init ModuleStatusProperty " << std::endl;
            device->moduleStatus.set("myModule1", MODULE_STATUS::NOT_AVAILABLE, context);
            device->moduleStatus.setCell( MODULE_STATUS::OK ,1,context);

            std::cout << "TRACE: " __FILE__ << ": " << __FUNCTION__ << ": Line " << __LINE__ << ": Init AcquisitionProperty " << std::endl;
            device->anAcquisitionField.set(3.14159, context);
            int64_t stamp = fesa::getSystemTime();
            device->acquisitionContext.insert(context, stamp);

        }
        catch (const fesa::FesaException& exception)
        {
            LOG_ERROR_IF(logger, exception.getMessage());
        }
    }
    std::cout << "TRACE: " __FILE__ << ": " << __FUNCTION__ << ": Line " << __LINE__ << " START " << std::endl;
}

} // FESAClassTemplate
