
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FESAClassTemplate_RealTimeAction_H_
#define _FESAClassTemplate_RealTimeAction_H_

#include <FESAClassTemplate/GeneratedCode/Device.h>
#include <FESAClassTemplate/GeneratedCode/GenRTActions.h>

namespace FESAClassTemplate
{

class RealTimeAction : public RealTimeActionBase
{
public:
    RealTimeAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~RealTimeAction();
    void execute(fesa::RTEvent* pEvt);
};

} // FESAClassTemplate

#endif // _FESAClassTemplate_RealTimeAction_H_
