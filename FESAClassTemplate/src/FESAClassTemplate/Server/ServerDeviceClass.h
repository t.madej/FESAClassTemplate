
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FESAClassTemplate_SERVER_DEVICE_CLASS_H_
#define _FESAClassTemplate_SERVER_DEVICE_CLASS_H_

#include <FESAClassTemplate/GeneratedCode/ServerDeviceClassGen.h>

namespace FESAClassTemplate
{

class ServerDeviceClass: public ServerDeviceClassGen
{
    public:
        static ServerDeviceClass* getInstance();
        static void releaseInstance();
        void specificInit();
        void specificShutDown();
    private:
        ServerDeviceClass();
        virtual  ~ServerDeviceClass();
        static ServerDeviceClass* instance_;
};

} // FESAClassTemplate

#endif // _FESAClassTemplate_SERVER_DEVICE_CLASS_H_
