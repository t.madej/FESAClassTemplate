
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _FESAClassTemplate_AcquisitionGetAction_H_
#define _FESAClassTemplate_AcquisitionGetAction_H_

#include <FESAClassTemplate/GeneratedCode/GetSetDefaultServerAction.h>
#include <FESAClassTemplate/GeneratedCode/PropertyData.h>
#include <FESAClassTemplate/GeneratedCode/TypeDefinition.h>
#include <FESAClassTemplate/GeneratedCode/Device.h>

namespace FESAClassTemplate
{

class AcquisitionGetAction : public AcquisitionGetActionBase
{
public:
    AcquisitionGetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~AcquisitionGetAction();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, AcquisitionPropertyData& data, const AcquisitionFilterData& filter);
    bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const AcquisitionFilterData& filter) const;
    bool isFilterValid(const fesa::AbstractDevice& abstractDevice, const AcquisitionFilterData& filter) const;
};

} // FESAClassTemplate

#endif // _FESAClassTemplate_AcquisitionGetAction_H_
