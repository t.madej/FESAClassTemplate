
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <FESAClassTemplate/Server/AcquisitionGetAction.h>
#include <FESAClassTemplate/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.FESAClassTemplate.Server.AcquisitionGetAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "FESAClassTemplate"; \
    diagMsg.name = "AcquisitionGetAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    FESAClassTemplateServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace FESAClassTemplate
{

AcquisitionGetAction::AcquisitionGetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        AcquisitionGetActionBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

AcquisitionGetAction::~AcquisitionGetAction()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void AcquisitionGetAction::execute(fesa::RequestEvent* pEvt, Device* pDev, AcquisitionPropertyData& data, const AcquisitionFilterData& filter)
{
	std::cout << "TRACE: " __FILE__ << ": " << __FUNCTION__ << ": Line " << __LINE__ << std::endl;
	data.setAcquisitionStamp(pDev->acquisitionContext.get(pEvt->getMultiplexingContext())->acquisitionStamp);
	int64_t aCycleStamp = 1234;
	std::cout << " Setting the GSI cycle stamp to " << aCycleStamp << std::endl;
	// both ways works to set the value of this item
	data.processStartStamp.set(aCycleStamp);
	data.sequenceStartStamp.set(aCycleStamp);
	std::cout << " processStartStamp " << data.processStartStamp.get() << std::endl;
	std::cout << " sequenceStartStamp " << data.sequenceStartStamp.get() << std::endl;
	data.setProcessIndex(pDev->acquisitionContext.get(pEvt->getMultiplexingContext())->processIndex);
	data.setSequenceIndex(pDev->acquisitionContext.get(pEvt->getMultiplexingContext())->sequenceIndex);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool AcquisitionGetAction::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const AcquisitionFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

// This method is called when a new subscription is requested. Returning false will abort the subscription.
bool AcquisitionGetAction::isFilterValid(const fesa::AbstractDevice& abstractDevice, const AcquisitionFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

} // FESAClassTemplate
